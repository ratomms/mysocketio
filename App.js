var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);


/*
app.get('/', function(req, res){
  res.send('<h2>Hello! This is  a Socket.IO demo</h2>');
});
*/

/*
io.on('connection', function (socket) {
	socket.emit('message', 'Vous êtes bien connecté !');
	socket.broadcast.emit('message', 'Un autre client vient de se connecter !');

	socket.on('message', function (message) {
		console.log('Un client me parle ! Il me dit : ' + message);
	});	
});
*/

console.log("Welcome to my Apps");
io.on('connection', function (socket) {
  console.log('A user connected '+socket.id);
  socket.emit('welcome', { msgContent: 'Welcome!', id: socket.id });
  
  socket.on('paymentstatus', function(msg){
    console.log('paymentstatus: ' + msg);
    //io.emit('paymentdone', msg);
    io.emit('paymentdone', {msgContent: 'Payment done',id_customer: 'NOT_SET_YET',id_driver: 'NOT_SET_YET',id_delivery: 'NOT_SET'});
    console.log('Message emited ');
  });
});


http.listen(9000, function(){
  console.log('listening on *:9000');
});
