<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version2X;


require '../vendor/autoload.php';

$config['db']['host']   = 'localhost';
$config['db']['user']   = 'tuto';
$config['db']['pass']   = 'Tuto123';
$config['db']['dbname'] = 'tutodb';

// $app = new \Slim\App;
$app = new \Slim\App(['settings' => $config]);

$container = $app->getContainer();

$container['db'] = function ($c) {
    $db = $c['settings']['db'];
    $pdo = new PDO('mysql:host=' . $db['host'] . ';dbname=' . $db['dbname'],
        $db['user'], $db['pass']);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

    return $pdo;
};

$app->get('/', function (Request $request, Response $response, array $args) {
    $response = $response->withHeader("Content-Type", "application/json");
    $response = $response->withStatus(200, "OK");
    $response = $response->withJson(array('msg'=>'This is a Tuto Client PHP for Socket.IO'));

    return $response;
});

$app->get('/dbstatus', function (Request $request, Response $response, array $args) {
    
    if($this->db){
         $response = $response->withHeader("Content-Type", "application/json");
         $response = $response->withStatus(200, "OK");
         $response = $response->withJson(array('msg'=>'Database connected'));
    }
    else {
         $response = $response->withHeader("Content-Type", "application/json");
         $response = $response->withStatus(200, "OK");
         $response = $response->withJson(array('msg'=>'Database disconnected'));
    }

    return $response;
});

$app->get('/setpayment/{id_request}/{id_customer}', function (Request $request, Response $response) {

    $id_customer = $request->getAttribute("id_customer");
    $id_request = $request->getAttribute("id_request");
    
    $sql = "UPDATE t_request SET payment_status = 'Paid' WHERE id_request=:id_request";
    $stmt = $this->db->prepare($sql);
    $stmt->bindParam(":id_request", $id_request);

    $isUpdated = $stmt->execute();

    if($isUpdated) {
	$id_driver = rand(10, 20);

	$sql = "INSERT INTO t_delivery(id_request,id_customer,id_driver) VALUES(:id_request,:id_customer,:id_driver)";
        $stmt = $this->db->prepare($sql);
	$stmt->bindParam(":id_request", $id_request);
	$stmt->bindParam(":id_customer", $id_customer);
	$stmt->bindParam(":id_driver", $id_driver);
        

        if($stmt->execute()){

	    $id_delivery = $this->db->lastInsertId();
	    
	    $data = array('msg' => 'Payment OK',
			  'id_request' => $id_request, 
			  'id_customer' => $id_customer,
		          'id_driver' => $id_driver,
			  'id_delivery' => $id_delivery);

	    $clientSocket = new Client(new Version2X('http://localhost:9000', [
	        		'headers' => [
					'X-My-Header: Smarto aka Umblah API',
					'Authorization: Bearer 12b3c4d5e6f7g8h9i'
	    			]
			]));

	    $clientSocket->initialize();
	    //$clientSocket->emit('paymentstatus', ['foo' => 'bar']);
	    $clientSocket->emit('paymentstatus', $data);
	    $clientSocket->close();
            
            $response = $response->withHeader("Content-Type", "application/json");
            $response = $response->withStatus(200, "OK");
            $response = $response->withJson($data);
            return $response;
        }
        else {
            $response = $response->withHeader("Content-Type", "application/json");
            $response = $response->withStatus(200, "OK");
            $response = $response->withJson(array('msg' => 'Payment KO','id_request' => $id_request,'id_customer' => $id_customer));
        }
        
    }
    else {
	$response = $response->withHeader("Content-Type", "application/json");
        $response = $response->withStatus(200, "OK");
        $response = $response->withJson(array('msg' => 'Payment KO','id_request' => $id_request,'id_customer' => $id_customer));

        return $response;
    }
});

$app->run();

